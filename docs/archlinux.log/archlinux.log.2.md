[Chap.1, 2 へ戻る](archlinux.log.md)

# archlinux.log

## Chap.3 ユーザーと GUI

### ユーザーを追加

```console
# useradd -m -g wheel [username]
# passwd [username]
Enter new UNIX password: [password]
Retype new UNIX password: [password]
```

### sudo

```console
# pacman -S sudo
# visudo
```

コマンド **visudo**  
Defaults env_keep += "HOME" と %wheel ALL=(ALL) ALL をアンコメント

### 新しいユーザーでログイン

```console
# exit
login: [username]
Password: [password]
```

### Git, yay のインストール

```console
$ sudo pacman -S git
$ mkdir /tmp/works
$ cd /tmp/works
$ git clone https://aur.archlinux.org/yay.git
$ cd yay
$ makepkg -si
```

### Powershell のインストール

```console
$ cd ..
$ yay -S powershell-git
```

### ログインシェルを変更

```console
$ chsh -s /usr/bin/pwsh
$ sudo reboot
```

### GNOME のインストール

```console
$ pacman -Sy --noconfirm gnome gnome-extra
$ sudo systemctl enable gdm
$ sudo reboot
```

## Chap.4 日本語化

### フォントのインストール

```console
$ yay -S ttf-genshin-gothic
$ yay -S ttf-ricty
$ sudo vim /etc/locale.conf
```

ファイル **/etc/locale.conf**
en_US を ja_JP に変更

### 日本語入力ソース Mozc

```console
$ sudo pacman -S ibus-anthy
$ yay -S mozc
```

GNOME の '設定 -> 地域と言語 -> 入力ソース'  
"+" から "日本語", "日本語 (Anthy)" を追加する

```console
$ sudo reboot
```

そして、再起動

[Chap.5 へ続く](archlinux.log.3.md)