# archlinux.log

### パーティション構造

デバイス名 : マウントポイント
- sda
    - sda1 : /boot
    - sda2 : /

## Chap.1 基本システム

### 日本語化

```console
# loadkeys jp106
```

### インターネットに接続

```console
# (ip link)
# (dhcpcd)
# ping -c3 archlinux.jp
```

### パーティション

```console
# parted
(papted) (p)
(parted) mkt msdos
(parted) y
(parted) (p)
(parted) mkp p 1M 2G
(parted) mkp p 2G 50G
(parted) (p)
(parted) q
```

### フォーマット

```console
# mkfs.xfs -f /dev/sda1
# mkfs.xfs -f /dev/sda2
# (parted -l)
```

### マウント

```console
# mount /dev/sda2 /mnt
# mkdir /mnt/boot
# mount /dev/sda1 /mnt/boot
```

### システムクロック

```console
# timedatectl set-ntp true
# (timedatectl)
```

### 基本システムの構築

```console
# pacman -Sy
# pacman -S archlinux-keyring
# pacstrap /mnt base base-devel (vim wget)
```

### fstab の生成

```console
# genfstab -U /mnt >> /mnt/etc/fstab
```

### chroot

```console
# arch-chroot /mnt bash
```

## Chap.2 Arch Linux の設定

### 時刻の設定

```console
# ln -sf /usr/share/zoneinfo/Asia/Tokyo /etc/localtime
# hwclock --systohc --utc
```

### ロケール

ファイル **/etc/locale.gen**  
en_US.UTF-8 UTF-8 と
ja_JP.UTF-8 UTF-8 をアンコメント

```console
# locale-gen
# echo LANG=en_US.UTF-8 > /etc/locale.conf
# echo KEYMAP=jp106 > /eac/vconsole.conf
```

### ホストネーム

```console
# echo Earth > /etc/hostname
# cat << $ >> /etc/hosts
> 127.0.0.1 localhost
> ::1 localhost
> $
```

### ネットワーク設定

```console
# systemctl enable dhcpcd
# (pacman -Sy)
# (pacman -S dialog)
# (wifi-menu)
```

### GRUB

```console
# pacman -S archlinux-keyring
# pacman -S grub os-prober
# grub-install /dev/sda
# grub-mkconfig -o /boot/grub/grub.cfg
```

### chroot から抜ける

```console
# exit
```

### 再起動

```console
# umount -R /mnt
# reboot
```
[Chap.3 へ続く](archlinux.log.2.md)