[Chap.3, 4 へ戻る](archlinux.log.2.md)

# archlinux.log

## Chap.5 インストール

### Firefox

```console
$ sudo pacman -S firefox
```

### p7zip

```console
$ sudo pacman -S p7zip
```

### Node.js

```console
$ yay -S nvm
$ nvm install --lts
```

### Yarn

```console
$ sudo pacman -S yarn
```

### chance-cli

```console
$ npm install -g chance-cli
```

### brackets

```console
$ yay -S brackets
```

### GitKraken

```console
$ yay -S gitkraken
```

### ZSH

```console
$ sudo pacman -S zsh
```

### zacman

```console
$ yay -S zacman
```